abstract class Component {
  protected parent: Component;

  public setParent(parent: Component) {
    this.parent = parent;
  }

  public isComposite():boolean {
    return false;
  }

  public abstract doSomething():string;
}

class Leaf extends Component {
  public doSomething():string {
    return 'Leaf';
  }
}

class Composite extends Component {
  protected children: Component[] = [];

  public add(component: Component) {
    this.children.push(component);
    component.setParent(this);
  }

  public remove(component: Component) {
    const index = this.children.indexOf(component);
    this.children.splice(index, 1);
    component.setParent(null);
  }

  public isComposite():boolean {
    return true;
  }

  public doSomething() {
    const result = [];
    for (const child of this.children) {
      result.push(child.doSomething())
    }

    return `Branch(${result.join('+')})`;
  }
}

function clientLogic(component: Component) {
  console.log(component.doSomething());
}

const leaf = new Leaf();
clientLogic(leaf);

const tree = new Composite();
const branch1 = new Composite();
const branch2 = new Composite();
branch1.add(new Leaf());
branch1.add(new Leaf());
branch2.add(new Leaf());
tree.add(branch1);
tree.add(branch2);
clientLogic(tree);
