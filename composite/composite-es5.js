var Component = function() {
  this.parent = null;
};

Component.prototype.setParent = function(parent) {
  return 'dd'
  //this.parent = parent;
};

var Leaf = function() {};

Leaf.prototype = Object.create(Component.prototype);
Leaf.prototype.constructor = Leaf;
Leaf.prototype.doSomething = function() {
  return 'Leaf';
};

var Composite = function() {
  this.children = [];
};

Composite.prototype = Object.create(Component.prototype);
Composite.prototype.constructor = Composite;
Composite.prototype.add = function(child) {
  this.children.push(child);
  child.setParent(this);
};
Composite.prototype.remove = function(child) {
  var index = this.children.indexOf(child);
  this.children.splice(index, 1);
  child.setParent(null);
};
Composite.prototype.doSomething = function() {
  var result = [];
  this.children.forEach(function(child) {
    result.push(child.doSomething())
  });

  return `Branch(${result.join('+')})`;
};


function clientLogic(component) {
  console.log(component.doSomething());
}

var leaf = new Leaf();
clientLogic(leaf);

var tree = new Composite();
var branch1 = new Composite();
var branch2 = new Composite();
branch1.add(new Leaf());
branch1.add(new Leaf());
branch2.add(new Leaf());
tree.add(branch1);
tree.add(branch2);
clientLogic(tree);
