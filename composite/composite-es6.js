class Component {
  constructor() {
    this.parent = null;
  }

  setParent(parent) {
    this.parent = parent;
  }

  isComposite() {
    return false;
  }

  doSomething() {}
}

class Leaf extends Component {
  doSomething() {
    return 'Leaf';
  }
}

class Composite extends Component {
  constructor() {
    super();
    this.children = [];
  }

  add(component) {
    this.children.push(component);
    component.setParent(this);
  }

  remove(component) {
    const index = this.children.indexOf(component);
    this.children.splice(index, 1);
    component.setParent(null);
  }

  isComposite() {
    return true;
  }

  doSomething() {
    const result = [];
    for (const child of this.children) {
      result.push(child.doSomething())
    }

    return `Branch(${result.join('+')})`;
  }
}

function clientLogic(component) {
  console.log(component.doSomething());
}

const leaf = new Leaf();
clientLogic(leaf);

const tree = new Composite();
const branch1 = new Composite();
const branch2 = new Composite();
branch1.add(new Leaf());
branch1.add(new Leaf());
branch2.add(new Leaf());
tree.add(branch1);
tree.add(branch2);
clientLogic(tree);
