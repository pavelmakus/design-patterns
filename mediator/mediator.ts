interface Mediator {
  notify(sender: object, event: string): void;
}

class ConcreteMediator implements Mediator {
  constructor(private c1: Component1, private c2: Component2) {
    this.c1.setMediator(this);
    this.c2.setMediator(this);
  }

  notify(sender: object, event: string): void {
    if (event === 'A') {
      console.log('event A');
      this.c2.doC();
    }

    if (event === 'D') {
      console.log('event D');
      this.c1.doB();
      this.c2.doC();
    }
  }
}

class BaseComponent {
  protected mediator: Mediator;

  setMediator(mediator: Mediator) {
    this.mediator = mediator;
  }
}

class Component1 extends BaseComponent {
  doA() {
    console.log('doA');
    this.mediator.notify(this, 'A')
  }

  doB() {
    console.log('doB');
    this.mediator.notify(this, 'B')
  }
}

class Component2 extends BaseComponent {
  doC() {
    console.log('doC');
    this.mediator.notify(this, 'C')
  }

  doD() {
    console.log('doD');
    this.mediator.notify(this, 'D')
  }
}

const c1 = new Component1();
const c2 = new Component2();
new ConcreteMediator(c1, c2);

c1.doA();
c2.doD();
