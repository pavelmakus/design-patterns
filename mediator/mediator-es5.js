function Mediator(c1, c2) {
  this.c1 = c1;
  this.c2 = c2;
  this.c1.setMediator(this);
  this.c2.setMediator(this);
}
Mediator.prototype.notify = function(sender, event) {
  if (event === 'A') {
    console.log('event A');
    this.c2.doC();
  }

  if (event === 'D') {
    console.log('event D');
    this.c1.doB();
    this.c2.doC();
  }
};

function BaseComponent() {
  this.mediator = null;
};
BaseComponent.prototype.setMediator = function(mediator) {
  this.mediator = mediator;
};

function Component1() {};
Component1.prototype = Object.create(BaseComponent.prototype);
Component1.prototype.constructor = Component1;
Component1.prototype.doA = function() {
  console.log('doA');
  this.mediator.notify(this, 'A')
};
Component1.prototype.doB = function() {
  console.log('doB');
  this.mediator.notify(this, 'B')
};

function Component2() {};
Component2.prototype = Object.create(BaseComponent.prototype);
Component2.prototype.constructor = Component2;
Component2.prototype.doC = function() {
  console.log('doC');
  this.mediator.notify(this, 'C')
};
Component2.prototype.doD = function() {
  console.log('doD');
  this.mediator.notify(this, 'D')
};


var c1 = new Component1();
var c2 = new Component2();
new Mediator(c1, c2);

c1.doA();
c2.doD();
