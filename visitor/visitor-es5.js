var Employee = function(salary) {
  var self = this;

  this.accept = function(visitor) {
    visitor.visit(self);
  };

  this.getSalary = function() {
    return salary;
  };

  this.setSalary = function(sal) {
    salary = sal;
  };
};

var ExtraSalary = function () {
  this.visit = function(emp) {
    emp.setSalary(emp.getSalary() * 1.1);
  };
};

function clientLogic() {
  var employees = [
    new Employee(10000),
    new Employee(20000),
  ];
  var visitorSalary = new ExtraSalary();

  for (var i = 0; i < employees.length; i++) {
    var emp = employees[i];
    emp.accept(visitorSalary);
    console.log(emp.getSalary())
  }
}

clientLogic();
