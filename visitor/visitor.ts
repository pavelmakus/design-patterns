interface MainComponent {
  accept(visitor: Visitor): void;
}

interface Visitor {
  visitComponentA(element: ComponentA): void;
  visitComponentB(element: ComponentB): void;
}

class ComponentA implements MainComponent {
  accept(visitor: Visitor): void {
    visitor.visitComponentA(this);
  }

  doSomething(): string {
    return 'ComponentA';
  }
}

class ComponentB implements MainComponent {
  accept(visitor: Visitor): void {
    visitor.visitComponentB(this);
  }

  doSomethingElse(): string {
    return 'ComponentB';
  }
}

class ConcreteVisitor implements Visitor {
  visitComponentA(element: ComponentA): void {
    console.log(element.doSomething());
  }
  visitComponentB(element: ComponentB): void {
    console.log(element.doSomethingElse());
  }
}

function clientLogic(components: MainComponent[], visitor: Visitor) {
  for (const component of components) {
    component.accept(visitor);
  }
}

const components = [new ComponentA(), new ComponentB()];
const visitor = new ConcreteVisitor();

clientLogic(components, visitor);
