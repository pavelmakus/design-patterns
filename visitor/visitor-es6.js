class ComponentA {
  accept(visitor) {
    visitor.visitComponentA(this);
  }

  doSomething() {
    return 'ComponentA';
  }
}

class ComponentB {
  accept(visitor) {
    visitor.visitComponentB(this);
  }

  doSomethingElse() {
    return 'ComponentB';
  }
}

class ConcreteVisitor {
  visitComponentA(element) {
    console.log(element.doSomething());
  }

  visitComponentB(element) {
    console.log(element.doSomethingElse());
  }
}

function clientLogic(components, visitor) {
  for (const component of components) {
    component.accept(visitor);
  }
}

const components = [new ComponentA(), new ComponentB()];
const visitor = new ConcreteVisitor();

clientLogic(components, visitor);
