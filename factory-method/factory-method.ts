interface Product {
  doSomething(): string;
}

abstract class Creator {
  abstract factoryMethod(): Product;

  someBusinessLogic(): string {
    const creator = this.factoryMethod();
    return creator.doSomething();
  }
}

class Creator1 extends Creator {
  public factoryMethod(): Product {
    return new Product1;
  }
}

class Creator2 extends Creator {
  public factoryMethod(): Product {
    return new Product2;
  }
}

class Product1 implements Product {
  doSomething(): string {
    return 'product1'
  }
}

class Product2 implements Product {
  doSomething(): string {
    return 'product2';
  }
}

function clientLogic(creator: Creator) {
  console.log(creator.someBusinessLogic());
}

clientLogic(new Creator1());
clientLogic(new Creator2());
