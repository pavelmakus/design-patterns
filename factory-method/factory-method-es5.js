function Foo() {
  //...
}

Foo.prototype.doSomething = function() {
  return 'foo';
};

function Bar() {
  //...
}

Bar.prototype.doSomething = function() {
  return 'bar';
};

function clientLogic(type) {
  switch(type) {
    case 'foo':
      return new Foo();
    case 'bar':
      return new Bar();
  }
}

console.log(clientLogic('foo').doSomething());
console.log(clientLogic('bar').doSomething());
