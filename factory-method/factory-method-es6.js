class Creator {
  someBusinessLogic() {
    const creator = this.factoryMethod();
    return creator.doSomething();
  }
}

class Creator1 extends Creator {
  factoryMethod() {
    return new Product1();
  }
}

class Creator2 extends Creator {
  factoryMethod() {
    return new Product2();
  }
}

class Product1 {
  doSomething() {
    return 'product1';
  }
}

class Product2 {
  doSomething() {
    return 'product2';
  }
}

function clientLogic(creator) {
  console.log(creator.someBusinessLogic())
}

clientLogic(new Creator1());
clientLogic(new Creator2());
