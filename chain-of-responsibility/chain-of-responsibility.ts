interface Handler {
  setNextHandler(handler: Handler): Handler;

  doSomething(request: string): string;
}

abstract class AbstractHandler implements Handler {
  private nextHandler: Handler;

  public setNextHandler(handler:Handler): Handler {
    this.nextHandler = handler;
    return handler;
  }

  public doSomething(request: string): string {
    if (this.nextHandler) {
      return this.nextHandler.doSomething(request);
    }

    return null;
  }
}

class Monkey extends AbstractHandler {
  public doSomething(request: string): string {
    if (request === 'banana') {
      return 'monkey - banana';
    }
    console.log(request);
    return super.doSomething(request);
  }
}

class Squirrel extends AbstractHandler {
  public doSomething(request: string): string {
    if (request === 'nut') {
      return 'squirrel - nut';
    }
    console.log(request);
    return super.doSomething(request);
  }
}

class Dog extends AbstractHandler {
  public doSomething(request: string): string {
    if (request === 'meat') {
      return 'dog - meat';
    }
    console.log(request);
    return super.doSomething(request);
  }
}


const monkey = new Monkey();
const squirrel = new Squirrel();
const dog = new Dog();

monkey.setNextHandler(squirrel).setNextHandler(dog);

console.log(monkey.doSomething('meat'));
console.log('**************');
console.log(squirrel.doSomething('banana'));
console.log('**************');
console.log(monkey.doSomething('banana'));
