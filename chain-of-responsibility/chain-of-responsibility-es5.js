var Request = function(amount) {
  this.amount = amount;
};

Request.prototype.get = function(bill) {
  var count = Math.floor(this.amount / bill);
  this.amount -= count * bill;
  console.log(count + " - $" + bill);
  return this;
};

new Request(378).get(100).get(50).get(20).get(10).get(5).get(1);
