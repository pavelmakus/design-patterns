class AbstractHandler {
  constructor() {
    this.nextHandler = null;
  }

  setNextHandler(handler) {
    this.nextHandler = handler;
    return handler;
  }

  doSomething(request) {
    if (this.nextHandler) {
      return this.nextHandler.doSomething(request);
    }

    return null;
  }
}

class Monkey extends AbstractHandler {
  doSomething(request) {
    if (request === 'banana') {
      return 'monkey - banana';
    }
    console.log(request);
    return super.doSomething(request);
  }
}

class Squirrel extends AbstractHandler {
  doSomething(request) {
    if (request === 'nut') {
      return 'squirrel - nut';
    }
    console.log(request);
    return super.doSomething(request);
  }
}

class Dog extends AbstractHandler {
  doSomething(request) {
    if (request === 'meat') {
      return 'dog - meat';
    }
    console.log(request);
    return super.doSomething(request);
  }
}


const monkey = new Monkey();
const squirrel = new Squirrel();
const dog = new Dog();

monkey.setNextHandler(squirrel).setNextHandler(dog);

console.log(monkey.doSomething('meat'));
console.log('**************');
console.log(squirrel.doSomething('banana'));
console.log('**************');
console.log(monkey.doSomething('banana'));
