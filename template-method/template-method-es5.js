function TemplateMethod() {}

TemplateMethod.prototype.doSomething = function() {
  this.step1();
  this.step2();
  this.hook1();
  this.step3();
  this.step4();
  this.step5();
  this.hook2();
};
TemplateMethod.prototype.step1 = function() {
  console.log('step 1');
};
TemplateMethod.prototype.step2 = function() {
  console.log('step 2');
};
TemplateMethod.prototype.step4 = function() {
  console.log('step 4');
};
TemplateMethod.prototype.step3 = function() {};
TemplateMethod.prototype.step5 = function() {};
TemplateMethod.prototype.hook1 = function() {};
TemplateMethod.prototype.hook2 = function() {};

function ConcreteClass1() {}
ConcreteClass1.prototype = Object.create(TemplateMethod.prototype);
ConcreteClass1.prototype.constructor = ConcreteClass1;
ConcreteClass1.prototype.step3 = function() {
  console.log('step 3 (ConcreteClass1)');
};
ConcreteClass1.prototype.step5 = function() {
  console.log('step 5 (ConcreteClass1)');
};
ConcreteClass1.prototype.hook1 = function() {
  console.log('hook 1 (ConcreteClass1)');
};

function ConcreteClass2() {}
ConcreteClass2.prototype = Object.create(TemplateMethod.prototype);
ConcreteClass2.prototype.constructor = ConcreteClass2;
ConcreteClass2.prototype.step2 = function() {
  console.log('step 2 (ConcreteClass2)');
};
ConcreteClass2.prototype.step3 = function() {
  console.log('step 3 (ConcreteClass2)');
};
ConcreteClass2.prototype.step5 = function() {
  console.log('step 5 (ConcreteClass2)');
};
ConcreteClass2.prototype.hook2 = function() {
  console.log('hook 2 (ConcreteClass2)');
};

function clientLogic(templateMethod) {
  templateMethod.doSomething();
}

clientLogic(new ConcreteClass1());
clientLogic(new ConcreteClass2());
