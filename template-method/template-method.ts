abstract class TemplateMethod {
  doSomething() {
    this.step1();
    this.step2();
    this.hook1();
    this.step3();
    this.step4();
    this.step5();
    this.hook2();
  }

  step1() {
    console.log('step 1')
  }

  step2() {
    console.log('step 2')
  }

  step4() {
    console.log('step 4')
  }

  abstract step3(): void;

  abstract step5(): void;

  hook1() {}

  hook2() {}
}

class ConcreteClass1 extends TemplateMethod {
  step3() {
    console.log('step 3 (ConcreteClass1)')
  }

  step5() {
    console.log('step 5 (ConcreteClass1)')
  }

  hook1() {
    console.log('hook 1 (ConcreteClass1)')
  }
}

class ConcreteClass2 extends TemplateMethod {
  step2() {
    console.log('step 2 (ConcreteClass2)')
  }

  step3() {
    console.log('step 3 (ConcreteClass2)')
  }

  step5() {
    console.log('step 5 (ConcreteClass2)')
  }

  hook2() {
    console.log('hook 2 (ConcreteClass2)')
  }
}

function clientLogic(templateMethod: TemplateMethod) {
  templateMethod.doSomething();
}

clientLogic(new ConcreteClass1());
clientLogic(new ConcreteClass2());
