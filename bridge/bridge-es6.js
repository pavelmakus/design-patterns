class Abstraction {
  constructor(implementation) {
    this.implementation = implementation;
  }

  doSomething() {
    return this.implementation.someMethod();
  }
}

class ConcreteImplementationA {
  someMethod() {
    return 'implementation A';
  }
}

class ConcreteImplementationB {
  someMethod() {
    return 'implementation B';
  }
}

function clientLogic() {
  const implementationA = new ConcreteImplementationA();
  const implementationB = new ConcreteImplementationB();
  let abstraction = new Abstraction(implementationA);
  console.log(abstraction.doSomething());
  abstraction = new Abstraction(implementationB);
  console.log(abstraction.doSomething());
}

clientLogic();
