var Mouse = function(output) {
  this.output = output;
  this.click = function () { return this.output.click(); };
  this.move = function () { return this.output.move(); };
};

var Screen = function() {
  this.click = function () { return "Screen select"; };
  this.move = function () { return "Screen move"; };
};

function clientLogic() {
  var screen = new Screen();
  var mouse = new Mouse(screen);

  console.log(mouse.click());
  console.log(mouse.move());
}

clientLogic();
