class Abstraction {
  constructor(private implementation: Implementation) {}

  doSomething():string {
    return this.implementation.someMethod();
  }
}

interface Implementation {
  someMethod(): string;
}

class ConcreteImplementationA implements Implementation {
  someMethod():string {
    return 'implementation A';
  }
}

class ConcreteImplementationB implements Implementation {
  someMethod():string {
    return 'implementation B';
  }
}

function clientLogic() {
  const implementationA = new ConcreteImplementationA();
  const implementationB = new ConcreteImplementationB();
  let abstraction = new Abstraction(implementationA);
  console.log(abstraction.doSomething());
  abstraction = new Abstraction(implementationB);
  console.log(abstraction.doSomething());
}

clientLogic();
