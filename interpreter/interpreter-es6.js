class GreetingEnglishService {
  interpret() {
    return "Hi";
  }
}

class GreetingFrenchService {
  interpret() {
    return "Salut";
  }
}

let greeter = null;
let userLanguage = "Fr";

if (userLanguage === "Fr") {
  greeter = new GreetingFrenchService();
} else {
  greeter = new GreetingEnglishService();
}

console.log(greeter.interpret());