interface Interpreter {
  interpret(): string
}

class GreetingEnglishService implements Interpreter {
  interpret(): string {
    return "Hi";
  }
}

class GreetingFrenchService implements Interpreter  {
  interpret(): string {
    return "Salut";
  }
}

let greeter: Interpreter = null;
let userLanguage = "Fr";

if (userLanguage === "Fr") {
  greeter = new GreetingFrenchService();
} else {
  greeter = new GreetingEnglishService();
}

console.log(greeter.interpret());