class Originator {
  constructor(private state: number) {}

  doSomething(): void {
    this.state = Math.random();
  }

  save(): Memento {
    return new ConcreteMemento(this.state);
  }

  restore(memento: Memento): void {
    this.state = memento.getState();
  }
}

interface Memento {
  getState(): number;
  getInfo(): string;
}

class ConcreteMemento implements Memento {
  date: Date;

  constructor(private state: number) {
    this.date = new Date();
  }

  getState() {
    return this.state;
  }

  getInfo() {
    return `${this.state} / ${this.date}`;
  }
}

class Caretaker {
  private mementos: Memento[] = [];

  constructor(private originator: Originator) {}

  backup(): void {
    this.mementos.push(this.originator.save());
  }

  undo(): void {
    this.originator.restore(this.mementos.pop())
  }

  showHistory(): void {
    for (const memento of this.mementos) {
      console.log(memento.getInfo());
    }
  }
}

const originator = new Originator(1);
const caretaker = new Caretaker(originator);

caretaker.backup();
originator.doSomething();

caretaker.backup();
originator.doSomething();

caretaker.backup();
originator.doSomething();

caretaker.showHistory();

caretaker.undo();
caretaker.showHistory();
