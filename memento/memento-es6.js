class Originator {
  constructor(state) {
    this.state = state;
  }

  doSomething() {
    this.state = Math.random();
  }

  save() {
    return new ConcreteMemento(this.state);
  }

  restore(memento) {
    this.state = memento.getState();
  }
}

class ConcreteMemento {
  constructor(state) {
    this.state = state;
    this.date = new Date();
  }

  getState() {
    return this.state;
  }

  getInfo() {
    return `${this.state} / ${this.date}`;
  }
}

class Caretaker {
  constructor(originator) {
    this.originator = originator
    this.mementos = []
  }

  backup() {
    this.mementos.push(this.originator.save());
  }

  undo() {
    this.originator.restore(this.mementos.pop())
  }

  showHistory() {
    for (const memento of this.mementos) {
    console.log(memento.getInfo());
  }
  }
}

const originator = new Originator(1);
const caretaker = new Caretaker(originator);

caretaker.backup();
originator.doSomething();

caretaker.backup();
originator.doSomething();

caretaker.backup();
originator.doSomething();

caretaker.showHistory();

caretaker.undo();
caretaker.showHistory();
