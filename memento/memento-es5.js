function Originator(state) {
  this.state = state;
}
Originator.prototype.doSomething = function() {
  this.state = Math.random();
};
Originator.prototype.save = function() {
  return new ConcreteMemento(this.state);
};
Originator.prototype.restore = function(memento) {
  this.state = memento.getState();
};

function ConcreteMemento(state) {
  this.state = state;
  this.date = new Date();
}
ConcreteMemento.prototype.getState = function() {
  return this.state;
};
ConcreteMemento.prototype.getInfo = function() {
  return this.state + ' / ' + this.date;
};

function Caretaker(originator) {
  this.originator = originator;
  this.mementos = [];
}
Caretaker.prototype.backup = function() {
  this.mementos.push(this.originator.save());
};
Caretaker.prototype.undo = function() {
  this.originator.restore(this.mementos.pop());
};
Caretaker.prototype.showHistory = function() {
  for (const memento of this.mementos) {
    console.log(memento.getInfo());
  }
};

var originator = new Originator(1);
var caretaker = new Caretaker(originator);

caretaker.backup();
originator.doSomething();

caretaker.backup();
originator.doSomething();

caretaker.backup();
originator.doSomething();

caretaker.showHistory();

caretaker.undo();
caretaker.showHistory();
