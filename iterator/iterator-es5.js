var Iterator = function(items) {
  this.index = 0;
  this.items = items;
};

Iterator.prototype.next = function() {
  return this.items[this.index++];
};
Iterator.prototype.hasNext = function() {
  return this.index < this.items.length;
};

function clientLogic(iterator) {
  while(iterator.hasNext()) {
    console.log(iterator.next())
  }
}

const array = ['A', 'B', 'C'];

clientLogic(new Iterator(array));
