# Итератор (Iterator)

**Итератор** — это _поведенческий_ паттерн проектирования, который даёт возможность последовательно обходить сложную коллекцию, без раскрытия деталей её реализации 

Преимущества:
  - Упрощает классы хранения данных
  - Позволяет реализовать различные способы обхода стуктуры данных
  - Позволяет одновременно перемещаться по структуре данных в разные стороны
  
Недостатки:
  - Неоправдан, если можно обойтись простым циклом
    
Пример, где можно встретить:
  - Где требуется обход разных коллекций