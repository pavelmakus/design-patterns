class BaseIterator {
  constructor(elements) {
    this.elements = elements;
    this.index = 0;
  }

  next() {
    return this.elements[this.index++];
  }

  hasNext() {
    return this.index < this.elements.length;
  }
}

class ArrayIterator extends BaseIterator {}

class ObjectIterator extends BaseIterator {
  constructor(elements) {
    super(elements);
    this.keys = Object.keys(elements);
  }

  next() {
   return this.elements[this.keys[this.index++]];
  }
}

function clientLogic(iterator) {
  while(iterator.hasNext()) {
    console.log(iterator.next())
  }
}

const array = ['A', 'B', 'C'];
const objects = [{letter: 'A'}, {letter: 'B'}, {letter: 'C'}];

clientLogic(new ArrayIterator(array));
clientLogic(new ObjectIterator(objects));
