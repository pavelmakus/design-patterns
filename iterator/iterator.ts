interface Iterator<T> {
  elements: T[];
  next(): T;
  hasNext(): boolean;
}

class BaseIterator<T> implements Iterator<T> {
  index = 0;

  constructor(public elements: T[]) {}

  next(): T {
    return this.elements[this.index++];
  }

  hasNext(): boolean {
    return this.index < this.elements.length;
  }
}

class ArrayIterator<T> extends BaseIterator<T> {}

class ObjectIterator<T> extends BaseIterator<T> {
  keys: string[] = [];

  constructor(public elements: T[]) {
    super(elements);
    this.keys = Object.keys(this.elements);
  }

  next(): T {
    return this.elements[this.keys[this.index++]];
  }
}

function clientLogic(iterator: Iterator<any>) {
  while(iterator.hasNext()) {
    console.log(iterator.next())
  }
}

const array: string[] = ['A', 'B', 'C'];
const objects: {letter: string}[] = [{letter: 'A'}, {letter: 'B'}, {letter: 'C'}];

clientLogic(new ArrayIterator<string>(array));
clientLogic(new ObjectIterator<{letter: string}>(objects));
