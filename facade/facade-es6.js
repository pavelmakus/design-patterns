class Facade {
  constructor(subsystem1, subsystem2) {
    this.subsystem1 = subsystem1;
    this.subsystem2 = subsystem2;
  }

  doSomeAction() {
    return this.subsystem1.doSomething() + this.subsystem2.doSomething();
  }
}

class Subsystem1 {
  doSomething() {
    return 5;
  }
}

class Subsystem2 {
  doSomething() {
    return 16;
  }
}

function clientLogic(facade) {
  console.log(facade.doSomeAction());
}

const facade = new Facade(new Subsystem1(), new Subsystem2());

clientLogic(facade);
