class Facade {
  constructor(private subsystem1: Subsystem1, private subsystem2: Subsystem2) {}

  doSomeAction(): number {
    return this.subsystem1.doSomething() + this.subsystem2.doSomething();
  }
}

class Subsystem1 {
  doSomething(): number {
    return 5;
  }
}

class Subsystem2 {
  doSomething(): number {
    return 16;
  }
}

function clientLogic(facade: Facade) {
  console.log(facade.doSomeAction());
}

const facade = new Facade(new Subsystem1(), new Subsystem2());

clientLogic(facade);
