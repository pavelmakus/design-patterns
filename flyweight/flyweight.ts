class Flyweight {
  constructor(private name: string) {}
}

class FlyweightFactory {
  objects: {[key: string]: Flyweight} = {};

  create(name: string) {
    let object = this.objects[name];
    if (object) return object;
    this.objects[name] = new Flyweight(name);
    return this.objects[name];
  }

  getObjects() {
    return this.objects;
  }
}

const factory = new FlyweightFactory();

factory.create('Object 1');
factory.create('Object 2');
factory.create('Object 3');
factory.create('Object 2');
factory.create('Object 2');

console.log(factory.getObjects());
