function Flyweight(name) {
  this.name = name;
};

var factory = (function() {
  var objects = {};

  return {
    create: function(name) {
      var object = objects[name];
      if (object) return object;
      objects[name] = new Flyweight(name);
      return objects[name];
    },

    getObjects: function() {
      return objects;
    }
  }
})();

factory.create('Object 1');
factory.create('Object 2');
factory.create('Object 3');
factory.create('Object 2');
factory.create('Object 2');

console.log(factory.getObjects());
