class Context {
  private state: State;

  constructor(state: State) {
    this.transitionTo(state);
  }

  transitionTo(state) {
    this.state = state;
    this.state.setContext(this);
  }

  request1() {
    this.state.handle1();
  }

  request2() {
    this.state.handle2();
  }
}

abstract class State {
  protected context: Context;

  setContext(context: Context) {
    this.context = context;
  }

  public abstract handle1(): void;

  public abstract handle2(): void;
}

class ConcreteState1 extends State {
  handle1() {
    console.log('handle1 - ConcreteState1');
    this.context.transitionTo(new ConcreteState2());
  }

  handle2() {
    console.log('handle2 - ConcreteState1');
  }
}

class ConcreteState2 extends State {
  handle1() {
    console.log('handle1 - ConcreteState2');
  }

  handle2() {
    console.log('handle2 - ConcreteState2');
    this.context.transitionTo(new ConcreteState1());
  }
}

const context = new Context(new ConcreteState1());
context.request1();
context.request2();
