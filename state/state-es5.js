function Context(state) {
  this.state = null;
  this.transitionTo(state);
}

Context.prototype.transitionTo = function(state) {
  this.state = state;
  this.state.setContext(this);
};
Context.prototype.request1 = function() {
  this.state.handle1();
};
Context.prototype.request2 = function() {
  this.state.handle2();
};

function State() {
  this.context = null;
}

State.prototype.setContext = function(context) {
  this.context = context;
};

function ConcreteState1() {}
ConcreteState1.prototype = Object.create(State.prototype);
ConcreteState1.prototype.constructor = ConcreteState1;
ConcreteState1.prototype.handle1 = function() {
  console.log('handle1 - ConcreteState1');
  this.context.transitionTo(new ConcreteState2());
};
ConcreteState1.prototype.handle2 = function() {
  console.log('handle2 - ConcreteState1');
};

function ConcreteState2() {}
ConcreteState2.prototype = Object.create(State.prototype);
ConcreteState2.prototype.constructor = ConcreteState2;
ConcreteState2.prototype.handle1 = function() {
  console.log('handle1 - ConcreteState2');
};
ConcreteState2.prototype.handle2 = function() {
  console.log('handle2 - ConcreteState2');
  this.context.transitionTo(new ConcreteState1());
};

const context = new Context(new ConcreteState1());
context.request1();
context.request2();
