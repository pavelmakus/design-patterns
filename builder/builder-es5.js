function Car() {
  this.doors = 0;

  this.addParts = function () {
    this.doors = 4;
  };

  this.showInfo = function () {
    return "This is a " + this.doors + "-door car";
  };
}

function Builder() {
  this.car = null;

  this.step1 = function() {
    this.car = new Car();
  };

  this.step2 = function() {
    this.car.addParts();
  };

  this.get = function() {
    return this.car;
  };
}

function Director() {
  this.setBuilder = function(builder) {
    builder.step1();
    builder.step2();
    return builder.get();
  }
}


function clientLogic() {
  var director = new Director();
  var builder = new Builder();
  var car = director.setBuilder(builder);

  console.log(car.showInfo());
}

clientLogic();