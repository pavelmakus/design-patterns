class Builder {
  constructor() {
    this.product = null;
    this.reset();
  }

  createProductA() {
    this.product.parts.push('A');
  }

  createProductB() {
    this.product.parts.push('B');
  }

  createProductC() {
    this.product.parts.push('C');
  }

  reset() {
    this.product = new Product();
  }

  getProduct() {
    const result = this.product;
    this.reset();
    return result;
  }
}

class Product {
  constructor() {
    this.parts = [];
  }

  listParts() {
    return `Parts: ${this.parts.join(', ')}`;
  }
}

class Director {
  constructor() {
    this.builder = null;
  }

  setBuilder(builder) {
    this.builder = builder;
  }

  buildMinProduct() {
    this.builder.createProductA();
  }

  buildFullProduct() {
    this.builder.createProductA();
    this.builder.createProductB();
    this.builder.createProductC();
  }
}

function clientLogic(director) {
  const builder = new Builder();
  director.setBuilder(builder);

  director.buildMinProduct();
  console.log(builder.getProduct().listParts());

  director.buildFullProduct();
  console.log(builder.getProduct().listParts());

  builder.createProductA();
  builder.createProductC();
  console.log(builder.getProduct().listParts());
}

clientLogic(new Director());
