interface Builder {
  createProductA(): void;
  createProductB(): void;
  createProductC(): void;
}

class ConcreteBuilder implements Builder {
  private product: Product;

  constructor() {
    this.reset();
  }

  createProductA(): void {
    this.product.parts.push('A');
  }

  createProductB(): void {
    this.product.parts.push('B');
  }

  createProductC(): void {
    this.product.parts.push('C');
  }

  reset(): void {
    this.product = new Product();
  }

  getProduct(): Product {
    const result = this.product;
    this.reset();
    return result;
  }
}

class Product {
  parts: string[] = [];

  listParts(): string {
    return `Parts: ${this.parts.join(', ')}`;
  }
}

class Director {
  private builder: Builder;

  setBuilder(builder): void {
    this.builder = builder;
  }

  buildMinProduct() {
    this.builder.createProductA();
  }

  buildFullProduct() {
    this.builder.createProductA();
    this.builder.createProductB();
    this.builder.createProductC();
  }
}

function clientLogic(director: Director) {
  const builder = new ConcreteBuilder();
  director.setBuilder(builder);

  director.buildMinProduct();
  console.log(builder.getProduct().listParts());

  director.buildFullProduct();
  console.log(builder.getProduct().listParts());

  builder.createProductA();
  builder.createProductC();
  console.log(builder.getProduct().listParts());
}

clientLogic(new Director());
