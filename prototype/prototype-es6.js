class Prototype {
  constructor() {
    this.primitive;
  }

  clone() {
    return Object.create(this);
  }
}

function clientLogic() {
  const p1 = new Prototype();
  p1.primitive = 88;
  const p2 = p1.clone();
  console.log(p1.primitive === p2.primitive)
}

clientLogic();
