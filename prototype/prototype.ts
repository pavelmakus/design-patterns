class Prototype {
  public primitive: any;

  public clone(): this {
    return Object.create(this);
  }
}

function clientLogic() {
  const p1 = new Prototype();
  p1.primitive = 79;
  const p2 = p1.clone();
  console.log(p2.primitive === p1.primitive);
}

clientLogic();
