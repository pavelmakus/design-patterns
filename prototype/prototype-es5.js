function Person(first, last) {
  this.first = first;
  this.last = last;
  this.say = function () {
    console.log("name: " + this.first + " " + this.last);
  };
  this.clone = function () {
    return Object.create(this)
  };
}

function clientLogic() {
  var p1 = new Person("n/a", "n/a");
  var p2 = p1.clone();
  console.log(p1.first === p2.first)
}

clientLogic();
