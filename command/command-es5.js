function add(x, y) { return x + y; }
function sub(x, y) { return x - y; }

var Command = function(execute, a, b) {
  this.execute = function() {
    return execute(a, b)
  };
};

var AddCommand = function(a, b) {
  return new Command(add, a, b);
};

var SubCommand = function(a, b) {
  return new Command(sub, a, b);
};


var Calculator = function() {
  this.execute = function (command) {
    return command.execute();
  }
};


function clientLogic() {
  var calculator = new Calculator();
  console.log(calculator.execute(new AddCommand(100, 55)));
  console.log(calculator.execute(new SubCommand(24, 7)));
}

clientLogic();
