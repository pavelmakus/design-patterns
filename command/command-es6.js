class FirstCommand  {
  execute() {
    return 'first';
  }
}

class SecondCommand {
  execute() {
    return 'second';
  }
}

class Invoker {
  setFirst(command) {
    this.first = command;
  }

  setSecond(command) {
    this.second = command;
  }

  doSomething() {
    return `${this.first.execute()} ${this.second.execute()}`;
  }
}

function clientLogic(invoker) {
  invoker.setFirst(new FirstCommand());
  invoker.setSecond(new SecondCommand());
  console.log(invoker.doSomething());

  invoker.setFirst(new SecondCommand());
  invoker.setSecond(new FirstCommand());
  console.log(invoker.doSomething());
}

clientLogic(new Invoker());
