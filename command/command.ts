interface Command {
  execute(): string;
}

class FirstCommand implements Command {
  execute(): string {
    return 'first';
  }
}

class SecondCommand implements Command {
  execute():string {
    return 'second';
  }
}

class Invoker {
  private first: Command;
  private second: Command;

  public setFirst(command: Command):void {
    this.first = command;
  }

  public setSecond(command:Command):void {
    this.second = command;
  }

  public doSomething():string {
    return `${this.first.execute()} ${this.second.execute()}`;
  }
}

function clientLogic(invoker: Invoker) {
  invoker.setFirst(new FirstCommand());
  invoker.setSecond(new SecondCommand());
  console.log(invoker.doSomething());

  invoker.setFirst(new SecondCommand());
  invoker.setSecond(new FirstCommand());
  console.log(invoker.doSomething());
}

clientLogic(new Invoker());
