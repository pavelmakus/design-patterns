interface Subject {
  someMethod(): string;
}

class RealSubject implements Subject {
  someMethod() {
    return 'Some text';
  }
}

class Proxy implements Subject {
  constructor(private subject: Subject) {}

  someMethod() {
    return Math.random() > 0.5 ? this.subject.someMethod() : 'Another text';
  }
}

function clientLogic() {
  const subject = new RealSubject();
  const proxy = new Proxy(subject);
  console.log(subject.someMethod());
  console.log(proxy.someMethod());
}

clientLogic();
