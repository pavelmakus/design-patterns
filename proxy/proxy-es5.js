function RealSubject() {
  this.someMethod = function() {
    return 'Some text';
  };
}

function Proxy(subject) {
  this.someMethod = function() {
    return Math.random() > 0.5 ? subject.someMethod() : 'Another text';
  };
}

function clientLogic() {
  var subject = new RealSubject();
  var proxy = new Proxy(subject);
  console.log(subject.someMethod());
  console.log(proxy.someMethod());
}

clientLogic();
