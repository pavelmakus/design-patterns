interface Strategy {
  execute(a: number, b: number): number;
}

class ConcreteStrategyAdd implements Strategy {
  execute(a: number, b: number): number {
    return a + b;
  }
}

class ConcreteStrategySubtract implements Strategy {
  execute(a: number, b: number): number {
    return a - b;
  }
}

class ConcreteStrategyMultiply implements Strategy {
  execute(a: number, b: number): number {
    return a * b;
  }
}

class Context {
  constructor(private strategy: Strategy) {}

  executeStrategy(a: number, b: number): number {
    return this.strategy.execute(a, b);
  }

  setStrategy(strategy: Strategy) {
    this.strategy = strategy;
  }
}

function clientLogic() {
  const add = new ConcreteStrategyAdd();
  const subtract = new ConcreteStrategySubtract();
  const multiply = new ConcreteStrategyMultiply();
  const context = new Context(add);

  console.log(context.executeStrategy(7, 8));
  context.setStrategy(subtract);
  console.log(context.executeStrategy(7, 8));
  context.setStrategy(multiply);
  console.log(context.executeStrategy(7, 8));
}

clientLogic();
