class ConcreteStrategyAdd {
  execute(a, b) {
    return a + b;
  }
}

class ConcreteStrategySubtract {
  execute(a, b) {
    return a - b;
  }
}

class ConcreteStrategyMultiply {
  execute(a, b) {
    return a * b;
  }
}

class Context {
  constructor(strategy) {
    this.strategy = strategy;
  }

  executeStrategy(a, b) {
    return this.strategy.execute(a, b);
  }

  setStrategy(strategy) {
    this.strategy = strategy;
  }
}

function clientLogic() {
  const add = new ConcreteStrategyAdd();
  const subtract = new ConcreteStrategySubtract();
  const multiply = new ConcreteStrategyMultiply();
  const context = new Context(add);

  console.log(context.executeStrategy(7, 8));
  context.setStrategy(subtract);
  console.log(context.executeStrategy(7, 8));
  context.setStrategy(multiply);
  console.log(context.executeStrategy(7, 8));
}

clientLogic();
