var Context = function(strategy) {
  this.strategy = strategy;
};

Context.prototype = {
  setStrategy: function(strategy) {
    this.strategy = strategy;
  },
  executeStrategy: function(a, b) {
    return this.strategy.execute(a, b);
  }
};

var ConcreteStrategyAdd = function() {
  this.execute = function(a, b) {
    return a + b;
  };
};

var ConcreteStrategySubtract = function() {
  this.execute = function(a, b) {
    return a - b;
  };
};

var ConcreteStrategyMultiply = function() {
  this.execute = function(a, b) {
    return a * b;
  };
};

function clientLogic() {
  var add = new ConcreteStrategyAdd();
  var subtract = new ConcreteStrategySubtract();
  var multiply = new ConcreteStrategyMultiply();
  var context = new Context(add);

  console.log(context.executeStrategy(7, 8));
  context.setStrategy(subtract);
  console.log(context.executeStrategy(7, 8));
  context.setStrategy(multiply);
  console.log(context.executeStrategy(7, 8));
}

clientLogic();
