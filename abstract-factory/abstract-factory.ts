interface AbstractFactory {
  createProductA(): ProductA;
  createProductB(): ProductB;
}

interface ProductA {
  doSomeAction(): string;
}

interface ProductB {
  doAnotherAction(): number;
}

class Factory1 implements AbstractFactory {
  createProductA(): ProductA {
    return new ProductA1();
  }

  createProductB(): ProductB {
    return new ProductB1()
  }
}

class ProductA1 implements ProductA {
  doSomeAction(): string {
    return 'prod a1';
  }
}

class ProductB1 implements ProductB {
  doAnotherAction(): number {
    return 1;
  }
}

class Factory2 implements AbstractFactory {
  createProductA(): ProductA {
    return new ProductA2();
  }

  createProductB(): ProductB {
    return new ProductB2()
  }
}

class ProductA2 implements ProductA {
  doSomeAction(): string {
    return 'prod a2';
  }
}

class ProductB2 implements ProductB {
  doAnotherAction(): number {
    return 2;
  }
}

function clientLogic(factory: AbstractFactory) {
  const productA = factory.createProductA();
  const productB = factory.createProductB();

  console.log(productA.doSomeAction());
  console.log(productB.doAnotherAction());
}

clientLogic(new Factory1());
clientLogic(new Factory2());
