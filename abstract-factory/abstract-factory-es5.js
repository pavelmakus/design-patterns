function AbstractFactory(type) {
  var factory;
  switch(type) {
    case 'factory1':
      factory = new Factory1();
      break;
    case 'factory2':
      factory = new Factory2();
      break;
    default:
      factory = new Factory1();
      break;
  }
  return factory;
}

function Factory1() {
  this.createProductA = function() {
    return new ProductA1();
  };

  this.createProductB = function() {
    return new ProductB1();
  };
}

function ProductA1() {
  this.doSomeAction = function () {
    return 'product a1';
  }
}

function ProductB1() {
  this.doAnotherAction = function () {
    return 'product b1';
  }
}

function Factory2() {
  this.createProductA = function() {
    return new ProductA2();
  };

  this.createProductB = function() {
    return new ProductB2();
  };
}

function ProductA2() {
  this.doSomeAction = function () {
    return 'product a2';
  }
}

function ProductB2() {
  this.doAnotherAction = function () {
    return 'product b2';
  }
}


function clientLogic(factory) {
  var myFactory =  new AbstractFactory(factory);
  var productA = myFactory.createProductA();
  var productB = myFactory.createProductB();

  console.log(productA.doSomeAction());
  console.log(productB.doAnotherAction());
}

clientLogic('factory1');
