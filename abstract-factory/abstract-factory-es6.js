class ProductA1 {
  doSomething() {
    return 'product a1';
  }
}

class ProductB1 {
  doSomethingTo() {
    return 'product b1';
  }
}

class ProductA2 {
  doSomething() {
    return 'product a2';
  }
}

class ProductB2 {
  doSomethingTo() {
    return 'product b2';
  }
}

class Factory1 {
  createProductA() {
    return new ProductA1();
  }

  createProductB() {
    return new ProductB1();
  }
}

class Factory2 {
  createProductA() {
    return new ProductA2();
  }

  createProductB() {
    return new ProductB2();
  }
}

function clientLogic(factory) {
  const productA = factory.createProductA();
  const productB = factory.createProductB();

  console.log(productA.doSomething());
  console.log(productB.doSomethingTo());
}

clientLogic(new Factory1());
clientLogic(new Factory2());
