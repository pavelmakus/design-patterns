class Observable {
  constructor() {
    this.observers = [];
    this.status = null;
  }

  attach(observer) {
    this.observers.push(observer);
  }

  detach(observer) {
    this.observers = this.observers.filter(value => observer !== value)
  }

  notify() {
    this.observers.forEach(observer => {
      observer.update(this.status);
    })
  }

  someBusinessLogic() {
    this.status = Math.random() > 0.5 ? 'online' : 'offline';
    this.notify();
  }
}

class Observer {
  update(status) {
    console.log(status)
  }
}

const observable = new Observable();
const observer1 = new Observer();
const observer2= new Observer();

observable.attach(observer1);
observable.attach(observer2);

observable.someBusinessLogic();
