var Observable = function() {
  var observers = [];
  var status;

  return {
    attach: function(observer) {
      observers.push(observer);
    },
    detach: function(observer) {
      var index = observers.indexOf(observer);
      observers.splice(index, 1);
    },
    notify: function(status) {
      observers.forEach(function(observer) {
        observer.update(status);
      });
    },
    someBusinessLogic: function() {
      status = Math.random() > 0.5 ? 'online' : 'offline';
      this.notify(status);
    }
  }
};

var Observer = function() {
  return {
    update: function(status) {
      // ...
    }
  };
};

var observable = new Observable();
var observer1 = new Observer();
var observer2 = new Observer();

observable.attach(observer1);
observable.attach(observer2);

observable.someBusinessLogic();
