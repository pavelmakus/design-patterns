interface Observer {
  update(status: string): void;
}

interface Observable {
  attach(observer: Observer): void;
  detach(observer: Observer): void;
  notify(): void;
}

class ObservableClass implements Observable {
  private status: string;
  private observers: Observer[] = [];

  attach(observer: Observer): void {
    this.observers.push(observer);
  }

  detach(observer: Observer): void {
    const index = this.observers.indexOf(observer);
    this.observers.splice(index, 1);
  }

  notify(): void {
    this.observers.forEach((observer: Observer) => {
      observer.update(this.status);
    })
  }

  public someBusinessLogic(): void {
    this.status = Math.random() > 0.5 ? 'online' : 'offline';
    this.notify();
  }
}

class ObserverClass implements Observer {
  update(status: string): void {
    // ...
  }
}

const observable = new ObservableClass();
const observer1 = new ObserverClass();
const observer2 = new ObserverClass();

observable.attach(observer1);
observable.attach(observer2);

observable.someBusinessLogic();
