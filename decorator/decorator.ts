interface Component {
  doSomething(): string;
}

class FirstComponent implements Component {
  doSomething() {
    return 'First Component';
  }
}

class Decorator implements Component {
  constructor(private component: Component) {}

  doSomething() {
    return this.component.doSomething();
  }
}

class DecoratorA extends Decorator {
  doSomething() {
    return 'DecoratorA: ' + super.doSomething();
  }
}

class DecoratorB extends Decorator {
  doSomething() {
    return 'DecoratorB: ' + super.doSomething();
  }
}

function clientLogic(component: Component) {
  console.log(component.doSomething());
}

const component = new FirstComponent();

clientLogic(new Decorator(component));
clientLogic(new DecoratorA(component));
clientLogic(new DecoratorB(component));
