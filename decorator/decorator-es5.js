var User = function(name) {
  this.name = name;

  this.say = function() {
    return "User: " + this.name;
  };
};

var DecoratedUser = function(user, city) {
  this.name = user.name;
  this.city = city;

  this.say = function() {
    return "Decorated User: " + this.name + ", " + this.city;
  };
};


function clientLogic(user) {
  console.log(user.say());
}

var user = new User("Kelly");
clientLogic(new User("Kelly"));
clientLogic(new DecoratedUser(user, "New York"));