class Component {
  doSomething() {
    return 'Component';
  }
}

class Decorator extends Component {
  constructor(component) {
    super();
    this.component = component;
  }

  doSomething() {
    return this.component.doSomething();
  }
}

class DecoratorA extends Decorator {
  doSomething() {
    return 'DecoratorA: ' + super.doSomething();
  }
}

class DecoratorB extends Decorator {
  doSomething() {
    return 'DecoratorB: ' + super.doSomething();
  }
}

function clientLogic(component) {
  console.log(component.doSomething());
}

const component = new Component();
clientLogic(new Decorator(component));
clientLogic(new DecoratorA(component));
clientLogic(new DecoratorB(component));
