class Target {
  someMethod() {
    return 'target';
  }
}

class Adaptee {
  differentMethod() {
    return 'adaptee';
  }
}

class Adapter extends Target {
  constructor(adaptee) {
    super();
    this.adaptee = adaptee;
  }

  someMethod() {
    return this.adaptee.differentMethod();
  }
}

function clientLogic(target) {
  console.log(target.someMethod());
}

clientLogic(new Target());
clientLogic(new Adapter(new Adaptee()));
