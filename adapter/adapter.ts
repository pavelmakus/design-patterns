interface ITarget {
  someMethod(): string;
}

interface IAdaptee {
  differentMethod(): string;
}

class Target implements ITarget{
  someMethod(): string {
    return 'target';
  }
}

class Adaptee implements IAdaptee {
  differentMethod(): string {
    return 'adaptee';
  }
}

class Adapter extends Target {
  constructor(private adaptee: IAdaptee) {
    super();
  }

  someMethod() {
    return this.adaptee.differentMethod();
  }
}

function clientLogic(target: ITarget) {
  console.log(target.someMethod());
}

clientLogic(new Target());
clientLogic(new Adapter(new Adaptee()));
