function Shipping() {
  this.request = function() {
    return "$49.75";
  }
}

function AdvancedShipping() {
  this.calculate = function() {
    return "$39.50";
  };
}

function ShippingAdapter() {
  var shipping = new AdvancedShipping();
  return {
    request: function() {
      return shipping.calculate();
    }
  };
}

function clientLogic(shipping) {
  console.log(shipping.request())
}

clientLogic(new Shipping());
clientLogic(new ShippingAdapter());
