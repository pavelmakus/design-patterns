var Singleton = (function () {
  function SingletonClass() {
    // ...
  }
  var instance = null;
  return {
    getInstance: function() {
      if (!instance) {
        instance = new SingletonClass();
        SingletonClass.prototype.constructor = null;
      }
      return instance;
    }
  }
})();
