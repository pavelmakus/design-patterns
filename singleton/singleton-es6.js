class Singleton {
  constructor() {
    // ...
  }

  method1() {
    // ...
  }

  method2() {
    // ...
  }
}

const singletonInstance = new Singleton();
Singleton.prototype.constructor = null;
Object.freeze(singletonInstance);

export { singletonInstance }
